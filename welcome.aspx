﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="welcome.aspx.cs" Inherits="welcome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <style>
        * {
            padding: 10px;
        }

        .label {
            display: inline-block;
        }

        .btn {
            display: inline-block;
            margin: 5px 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="panel panel-default">
      <div class="panel-heading">Geef je code in:</div>
      <div class="panel-body">
        <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="btnControleer" runat="server" OnClick="btnControleer_Click" Text="Controleer code" CssClass="btn btn-default" />
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
      </div>
    </div>
    </form>
</body>
</html>
