﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class welcome : System.Web.UI.Page
{
    protected void btnControleer_Click(object sender, EventArgs e)
    {
        BORRIAS_Ticket ticket = new BORRIAS_Ticket();
        BLLticket BLLticket = new BLLticket();

        ticket.code = txtCode.Text;
        ticket.used = 1;

        try
        {
            BLLticket.update(ticket);

            lblMessage.CssClass = "label label-success";
            lblMessage.Text = "Aanwezigheid opgenomen!";
        }
        catch (Exception error)
        {
            lblMessage.CssClass = "label label-danger";
            lblMessage.Text = error.Message;
        }
        
        
    }
}