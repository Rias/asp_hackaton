﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="createticket.aspx.cs" Inherits="createticket" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <style>
        * {
    padding: 10px;

        }
         .label {
    display: inline-block;
    width: 30px;
    }
        .btn {
    display: inline-block;
    margin: 5px 0;

        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="panel panel-default">
     <div class="panel-heading">Schrijf je in:</div>
      <div class="panel-body">
        <asp:Label ID="lblVoornaam" runat="server" AssociatedControlID="txtVoornaam" Text="Voornaam" CssClass="label label-default"></asp:Label>
        <asp:TextBox ID="txtVoornaam" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lblNaam" runat="server" AssociatedControlID="txtNaam" Text="Naam" CssClass="label label-default"></asp:Label>
        <asp:TextBox ID="txtNaam" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="btnVoegToe" runat="server" OnClick="btnVoegToe_Click" Text="Voeg toe" CssClass="btn btn-default" />
        <br />
        <asp:Label ID="lblStatus" runat="server"></asp:Label>
        </div>
        </div>
    </form>
</body>
</html>
