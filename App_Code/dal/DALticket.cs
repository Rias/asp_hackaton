﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DALticket
/// </summary>
public class DALticket
{
    private dcTicketDataContext dc = new dcTicketDataContext();

    // INSERT

    public void insert(BORRIAS_Ticket p_ticket)
    {
        dc.BORRIAS_Tickets.InsertOnSubmit(p_ticket);
        dc.SubmitChanges();
    }

    public void update(BORRIAS_Ticket p_ticket)
    {
        // Query the database for the row to be updated.
        var query =
            from t in dc.BORRIAS_Tickets
            where t.code == p_ticket.code
            select t;

        List<BORRIAS_Ticket> x = query.ToList();

        // Execute the query, and change the column values
        // you want to change.
        if (x.Count > 0)
        {
            foreach (BORRIAS_Ticket ticket in query)
            {
                ticket.used = 1;
            }
        }
        else
        {
            throw new Exception("Code niet gevonden");
        }
        // Submit the changes to the database.
        try
        {
            dc.SubmitChanges();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            // Provide for exceptions.
        }

    }

    public List<BORRIAS_Ticket> getAllUsed()
    {
        var result = from t in dc.BORRIAS_Tickets
                     where t.used == 1
                     select t;
        return result.ToList();
    }


}