﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="attendees.aspx.cs" Inherits="attendees" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Attendees</title>
    <link href="Content/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <style>
        form {
        width: 960px;
        margin: 0 auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager> 
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           
            <ContentTemplate>


        <h1>Aanwezige leden:</h1>
        <asp:Repeater ID="rptAttendees" runat="server" DataSourceID="objdsTicket">
            <HeaderTemplate>
                <table class="table">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("naam") %></td>
                    <td><%# Eval("voornaam")  %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <asp:ObjectDataSource ID="objdsTicket" runat="server" SelectMethod="getAllUsed" TypeName="BLLticket"></asp:ObjectDataSource>


                </ContentTemplate>
        </asp:UpdatePanel>


        <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
        </asp:Timer>


    </div>
    </form>
</body>
</html>
